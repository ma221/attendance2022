package fi.vamk.tka.demo2022;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AttendanceController {
	
	@GetMapping("/")
	public String swagger() {
		return "<script>window.location.replace(\"/swagger-ui.html\")</script>";
	}
	
	@GetMapping("/test")
	public String test() {
		return "{'id':1}";
	}
}
